import type { Product } from './Product'

const defaultReceiptItem = {
  id: -1,
  name: '',
  price: 0,
  unit: 0,
  productId: -1,
  product: null
}
type ReceiptItem = {
  map(arg0: (item: any) => { productId: any; qty: any }): { productId: number; qty: number }[]
  id: number
  name: string
  price: number
  unit: number
  productId: number
  product?: Product
}

export { type ReceiptItem, defaultReceiptItem }
